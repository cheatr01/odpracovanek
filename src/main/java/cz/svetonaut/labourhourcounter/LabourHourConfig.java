package cz.svetonaut.labourhourcounter;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class LabourHourConfig {

    public static final Double DEFAULT_HOURS_PER_DAY = 8.0;
    public static final Set<DayOfWeek> DEFAULT_WORK_DAYS_IN_WEEK = new HashSet<>(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY));

    private final Double workHoursPerDay;
    private final Set<DayOfWeek> workDaysPerWeek;

    public LabourHourConfig( Double workHoursPerDay, Set<DayOfWeek> workDaysPerWeek) {
        this.workHoursPerDay = Optional.ofNullable(workHoursPerDay).orElse(DEFAULT_HOURS_PER_DAY);
        this.workDaysPerWeek = Optional.ofNullable(workDaysPerWeek).orElse(DEFAULT_WORK_DAYS_IN_WEEK);
    }

    public LabourHourConfig() {
        this(null, null);
    }

    public Double getWorkHoursPerDay() {
        return workHoursPerDay;
    }

    public Set<DayOfWeek> getWorkDaysPerWeek() {
        return workDaysPerWeek;
    }

    public LabourHourConfig setWorkHoursPerDay(Double hours) {
        return new LabourHourConfig(hours, this.workDaysPerWeek);
    }

    public LabourHourConfig setWorkDaysPerWeek(Set<DayOfWeek> days) {
        return new LabourHourConfig(this.workHoursPerDay, days);
    }
}
