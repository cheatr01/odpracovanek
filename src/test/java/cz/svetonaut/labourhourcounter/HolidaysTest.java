package cz.svetonaut.labourhourcounter;

import java.time.LocalDate;
import java.time.Month;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HolidaysTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void holidaysTest(){
        var instance = new Holidays(2020);
        var easterSundayDate = instance.getEasterSundayDate(2020);
        Assertions.assertEquals(LocalDate.of(2020, Month.APRIL, 12), easterSundayDate);
    }
}