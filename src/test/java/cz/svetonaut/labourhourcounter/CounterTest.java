package cz.svetonaut.labourhourcounter;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.HashSet;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CounterTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void countTest() {
        Counter instance = new Counter(prepareConfigWithoutFriday());
        var result = instance.count(LocalDate.of(2020, Month.JULY, 2));
        var checkResult = new CounterResult(Duration.ofHours(8), Duration.ofHours(136));
        assertEquals(checkResult.getWorkedToToday(), result.getWorkedToToday());
        assertEquals(checkResult.getMonthWork(), result.getMonthWork());
    }

    @Test
    public void countTestEasters() {
        Counter instance = new Counter(new LabourHourConfig());
        var result = instance.count(LocalDate.of(2020, Month.APRIL, 14));
        var checkResult = new CounterResult(Duration.ofHours(7*8), Duration.ofHours(20*8));
        assertEquals(checkResult.getWorkedToToday(), result.getWorkedToToday());
        assertEquals(checkResult.getMonthWork(), result.getMonthWork());
    }

    @Test
    public void countTestOrdinaryFebruary() {
        Counter instance = new Counter(prepareConfigWithoutFriday());
        var count = instance.count(LocalDate.of(2019, Month.FEBRUARY, 6));
        var checkResult = new CounterResult(Duration.ofHours(16), Duration.ofHours(128));
        assertEquals(checkResult.getWorkedToToday(), count.getWorkedToToday());
        assertEquals(checkResult.getMonthWork(), count.getMonthWork());
    }

    @Test
    public void countTestLeapYearFebruary() {
        Counter instance = new Counter(prepareConfigWithoutFriday());
        var count = instance.count(LocalDate.of(2016, Month.FEBRUARY, 6));
        var checkResult = new CounterResult(Duration.ofHours(32), Duration.ofHours(136));
        assertEquals(checkResult.getWorkedToToday(), count.getWorkedToToday());
        assertEquals(checkResult.getMonthWork(), count.getMonthWork());
    }

    @Test
    public void countTestWithNullAsConfig() {
        Counter instance = new Counter(null);
        CounterResult result = instance.count(LocalDate.of(2020, Month.JULY, 2));
        var checkResult = new CounterResult(Duration.ofHours(8), Duration.ofHours(168));
    }

    @Test
    public void countTestWithNullAsDate() {
        Counter instance = new Counter(prepareConfigWithoutFriday());
        assertThrows(IllegalArgumentException.class, () -> {
            CounterResult result = instance.count(null);
        });
    }

    private LabourHourConfig prepareConfigWithoutFriday() {
        HashSet<DayOfWeek> dayOfWeeks = new HashSet<>(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY));
        return new LabourHourConfig(null, dayOfWeeks);
    }
}