package cz.svetonaut.labourhourcounter;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;

class Holidays {

    private Set<LocalDate> holidays = new HashSet<>();

    public Holidays(int year) {
        prepareHolidays(year);
    }

    private void prepareHolidays(int year) {
        holidays.add(LocalDate.of(year, Month.JANUARY, 1));
        holidays.add(LocalDate.of(year, Month.MAY, 1));
        holidays.add(LocalDate.of(year, Month.MAY, 5));
        holidays.add(LocalDate.of(year, Month.JULY, 5));
        holidays.add(LocalDate.of(year, Month.JULY, 6));
        holidays.add(LocalDate.of(year, Month.SEPTEMBER, 28));
        holidays.add(LocalDate.of(year, Month.OCTOBER, 28));
        holidays.add(LocalDate.of(year, Month.NOVEMBER, 11));
        holidays.add(LocalDate.of(year, Month.DECEMBER, 24));
        holidays.add(LocalDate.of(year, Month.DECEMBER, 25));
        holidays.add(LocalDate.of(year, Month.DECEMBER, 26));
        holidays.add(getEasterSundayDate(year).plus(1, ChronoUnit.DAYS));
        holidays.add(getEasterSundayDate(year).minus(2, ChronoUnit.DAYS));
    }

    public boolean contains(LocalDate day) {
        return holidays.contains(day);
    }

    protected LocalDate getEasterSundayDate(int year){
        int a,b,c,d,e,g,h,j,k,m,r,n,p;
        a = year % 19;
        b = year / 100;
        c = year % 100;
        d = b / 4;
        e = b % 4;
        g = (8 * b + 13) / 25;
        h = (19 * a + b - d - g + 15) % 30;
        j = c / 4;
        k = c % 4;
        m = (a + 11 * h) / 319;
        r = (2 * e + 2 * j - k - h + m + 32) % 7;
        n = (h - m + r + 90) / 25;
        p = (h - m + r + n + 19) % 32;
        return LocalDate.of(year, n, p);
    }
}
