package cz.svetonaut.labourhourcounter;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;
class Counter {

    private LabourHourConfig config;
    private Holidays holidays;
    private LocalDate day;

    public Counter(LabourHourConfig config) {
        this.config = Optional.ofNullable(config).orElse(new LabourHourConfig());
    }

    public CounterResult count(LocalDate today) {
        if(today == null){
            throw new IllegalArgumentException("We get NULL instead LocalDate");
        }
        holidays = new Holidays(today.getYear());
        Month month = today.getMonth();
        final Set<DayOfWeek> workDaysPerWeek = config.getWorkDaysPerWeek();

        day = today.minus(today.getDayOfMonth() - 1, ChronoUnit.DAYS);
        int numberOfWorkDays = 0;
        int hoursToToday = 0;
        while (month == day.getMonth()) {
            if (day.equals(today)) {
                hoursToToday = numberOfWorkDays;
            }
            if (!workDaysPerWeek.contains(day.getDayOfWeek()) || holidays.contains(day)) {
                plusOneDay();
                continue;
            }
            numberOfWorkDays++;
            plusOneDay();
        }
        return new CounterResult(
                Duration.of((long) (hoursToToday * config.getWorkHoursPerDay() * 60), ChronoUnit.MINUTES),
                Duration.of((long) (numberOfWorkDays * config.getWorkHoursPerDay() * 60), ChronoUnit.MINUTES)
        );
    }

    private void plusOneDay() {
        day = day.plus(1, ChronoUnit.DAYS);
    }
}
