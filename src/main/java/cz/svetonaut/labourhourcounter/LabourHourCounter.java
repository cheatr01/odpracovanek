package cz.svetonaut.labourhourcounter;

import java.time.LocalDate;

public class  LabourHourCounter {

    private LabourHourConfig config;

    public LabourHourCounter(LabourHourConfig config) {
        this.config = config;
    }

    public CounterResult count(LocalDate date) {
        Counter counter = new Counter(config);
        return counter.count(date);
    }
}
