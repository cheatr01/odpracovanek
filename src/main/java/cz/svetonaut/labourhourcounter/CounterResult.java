package cz.svetonaut.labourhourcounter;

import java.time.Duration;

public class CounterResult {

    private final Duration workedToToday;
    private final Duration monthWork;

    public CounterResult(Duration workedToToday, Duration monthWork) {
        this.workedToToday = workedToToday;
        this.monthWork = monthWork;
    }

    public Duration getWorkedToToday() {
        return workedToToday;
    }

    public Duration getMonthWork() {
        return monthWork;
    }
}
